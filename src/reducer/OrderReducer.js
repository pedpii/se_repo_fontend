const initialState = {
    order_number:"",
    order_date:"",
    order_saleman:"",
    payment_select:"",
    payment_money:"",
    payment_date:"",
    payment_time:"",
    payment_name:"",
    payment_agency:"",
    send_name:"",
    send_address:"",
    send_postcode:"",
    send_phone:"",
    bearer_name:"",
    bearer_address:"",
    bearer_postcode:"",
    bearer_phone:"",
    pro_id:"",
    pro_name:"",
    pro_unit:"",
    pro_amount:1,
    pro_price:0,
    pro_cost:0,
    pro_tax:0,       
    pro_total:0,
    pro_payment:0
}

const OrderReducer = (state = initialState, action) => {
    switch (action.type) {
        case "DATA_GENERAL":
            state = {
                ...state,
                order_number:action.payload.order_number,
                order_date:action.payload.order_date,
                order_saleman:action.payload.order_saleman,
                payment_select:action.payload.payment_select,
                payment_money:action.payload.payment_money,
                payment_date:action.payload.payment_date,
                payment_time:action.payload.payment_time,
                payment_name:action.payload.payment_name,
                payment_agency:action.payload.payment_agency,
                
            }
        break;
        case "DATA_ADDRESS":
            state = {
                ...state,
                send_name:action.payload.send_name,
                send_address:action.payload.send_address,
                send_postcode:action.payload.send_postcode,
                send_phone:action.payload.send_phone,
                bearer_name:action.payload.bearer_name,
                bearer_address:action.payload.bearer_address,
                bearer_postcode:action.payload.bearer_postcode,
                bearer_phone:action.payload.bearer_phone
            }
        break;
        
        case "PRODUCT_DATA":
            state = {
                ...state,
                pro_id:action.payload.pro_id,
                pro_name:action.payload.pro_name,
                pro_unit:action.payload.pro_unit,
                pro_amount:action.payload.pro_amount,
                pro_price:action.payload.pro_price,
                pro_cost:action.payload.pro_cost, // pro_cost = amount*price
                pro_total:action.payload.pro_total, //pro_total += pro_cost
                pro_tax:action.payload.pro_tax, //pro_tax = pro_cost*0.07   
                pro_payment:action.payload.pro_payment //pro_payment = pro_total+pro_tax
            }
        break;

        case "ADD_DATA":
            state = {
                ...state,
                pro_id:action.payload.pro_id,
                pro_name:action.payload.pro_name,
                pro_unit:action.payload.pro_unit,
                pro_amount:action.payload.pro_amount,
                pro_price:action.payload.pro_price,
                pro_cost:action.payload.pro_amount*action.payload.pro_price, // pro_cost = amount*price
                pro_total:action.payload.pro_total+action.payload.pro_cost, //pro_total += pro_cost
                pro_tax:action.payload.pro_total*0.07, //pro_tax = pro_total*0.07   
                pro_payment:action.payload.pro_payment //pro_payment = pro_total+pro_tax
            }
        break;

        case "DELETE_DATA":
            state = {
                ...state,
                pro_id:action.payload.pro_id,
                pro_name:action.payload.pro_name,
                pro_unit:action.payload.pro_unit,
                pro_amount:action.payload.pro_amount,
                pro_price:action.payload.pro_price,
                pro_cost:action.payload.pro_amount*action.payload.pro_price, // pro_cost = amount*price
                pro_total:action.payload.pro_total-action.payload.pro_cost, //pro_total += pro_cost
                pro_tax:action.payload.pro_total*0.07, //pro_tax = pro_total*0.07   
                pro_payment:action.payload.pro_payment //pro_payment = pro_total+pro_tax
            }
        break;
    }
    

    return state;
}


export default OrderReducer;