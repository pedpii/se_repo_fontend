import { combineReducers } from 'redux'

import OrderReducer from './OrderReducer';

const rootReducer = combineReducers({
    OrderReducer : OrderReducer
});

export default rootReducer;