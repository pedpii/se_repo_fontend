import React, { Component } from 'react';
import { get, ip } from '../service/service';
import { user_token } from '../support/Constance';
import { Table ,CardImg} from 'reactstrap';

class Home extends Component {
    constructor(props) {
        super(props);
        this.state = {
            product_data: [],
            pro_name: null,
            pro_amount: null,
            pro_details: null,
            pro_price: null,
            pro_status: null,
            pro_image: null,
            default_image: "https://muaythaiauthority.com/wp-content/uploads/2014/10/default-img.gif"
        };
    }

    componentWillMount() {
        this.get_product()
    }

    get_product = async () => {
        try {
            await get('product/show_product', user_token).then((result) => {
                if (result.success) {
                    this.setState({
                        product_data: result.result
                    })
                    // this.datamap()

                    setTimeout(() => {
                        console.log("get_product", result.result)
                    }, 500)
                } else {
                    //window.location.href = "/";
                    alert(result.error_message)
                }
            });
        } catch (error) {
            alert("get_product2" + error);
        }
    }

    render() {
        return (
            <div className="App">
                <h1> หน้าแรก </h1>
                <h3> สินค้ายอดนิยม </h3>
                <h3> สินค้าอื่น </h3>
                <Table hover>
                    <tbody>
                        <tr>
                            <th>ลำดับ</th>
                            <th>รหัสสินค้า</th>
                            <th>ชื่อสินค้า</th>
                            <th>สถานะ</th>
                            <th>ราคาขาย</th>
                            <th>จำนวนในสต๊อก</th>
                            <th>รายละเอียด</th>
                            <th>รูปสินค้า</th>
                        </tr>
                        {
                            this.state.product_data.map((element, index) => {
                                return (
                                    <tr>
                                        <td>{index+1}</td>
                                        <td>SE_{element.pro_id}</td>
                                        <td>{element.pro_name}</td>
                                        <td>{element.pro_status}</td>
                                        <td>{element.pro_price}</td>
                                        <td>{element.pro_amount}</td>
                                        <td>{element.pro_details}</td>
                                        <td><CardImg top width="50%" src={element.pro_image ? ip+element.pro_image : this.state.default_image}/></td>
                                    </tr>
                                )
                            })
                        }
                    </tbody>
                </Table>


            </div>
        )
    }
}

export default Home;