import React, { Component } from 'react';
import { withRouter, Link } from 'react-router-dom';
import {
    Collapse,
    Navbar,
    NavbarToggler,
    NavbarBrand,
    Nav,
    NavItem,
    NavLink,
    UncontrolledDropdown,
    DropdownToggle,
    DropdownMenu,
    DropdownItem
} from 'reactstrap';


class Nav_bar extends Component {
    constructor(props) {
        super(props);

        this.toggleNavbar = this.toggleNavbar.bind(this);
        this.state = {
            isOpan: false
        };
    }

    toggleNavbar() {
        this.setState({
            isOpan: !this.state.collapsed
        });
    }


    logOut = (e) => {
        e.preventDefault()
        localStorage.removeItem('user_token')
        this.props.history.push('/')
    }

    render() {
        const loginRegLink = (
            <div>
                <Navbar color="light" light expand="md">
                    <NavbarBrand href="/">SE Shop</NavbarBrand>
                    <NavbarToggler onClick={this.toggle} />
                    <Collapse isOpan={!this.state.collapsed} navbar>
                        <Nav className="ml-auto" navbar>
                            <NavItem>
                                <NavLink href="/"  >
                                    หน้าแรก
                                </NavLink>
                            </NavItem>
                            <NavItem>
                                <NavLink href="/signin" >
                                    เข้าสู่ระบบ
                                </NavLink>

                            </NavItem>

                            <NavItem>
                                <NavLink href="/signup">
                                    ลงทะเบียน
                                </NavLink>
                            </NavItem>
                        </Nav>
                    </Collapse>
                </Navbar>
            </div >
        )

        const userLink = (
            <div>
                <Navbar color="light" light expand="md">
                    <NavbarBrand href="/">SE Shop</NavbarBrand>
                    <NavbarToggler onClick={this.toggle} />
                    <Collapse isOpan={!this.state.collapsed} navbar>
                        <Nav className="ml-auto" navbar>
                            <NavItem>
                                <Link to="/">
                                    <NavLink  >
                                        หน้าแรก
                                </NavLink>
                                </Link>
                            </NavItem>

                            <UncontrolledDropdown nav inNavbar>
                                <DropdownToggle nav caret> ข้อมูลผู้ใช้ </DropdownToggle>
                                <DropdownMenu right>
                                    <DropdownItem>
                                        <NavLink href="/user"> ข้อมูลผู้ใช้ </NavLink>
                                    </DropdownItem>

                                    <UncontrolledDropdown nav inNavbar>
                                        <DropdownToggle nav caret> แก้ไขข้อมูล </DropdownToggle>
                                        <DropdownMenu right>
                                            <DropdownItem>
                                                <NavLink href="/edit_profile"> ข้อมูลผู้ใช้ </NavLink>
                                            </DropdownItem>

                                            <DropdownItem>
                                                <NavLink href="/edit_password"> รหัสผ่าน </NavLink>
                                            </DropdownItem>

                                        </DropdownMenu>
                                    </UncontrolledDropdown>
                                </DropdownMenu>
                            </UncontrolledDropdown>

                            <NavItem>
                                <Link to="/product">
                                    <NavLink  >
                                        สินค้า
                                </NavLink>
                                </Link>
                            </NavItem>
                            
                            <NavItem>
                                <Link to="/manage_order">
                                    <NavLink  >
                                        จัดการใบสั่งซื้อ
                                </NavLink>
                                </Link>
                            </NavItem>

                            <NavItem>
                                <NavLink href="/" onClick={this.logOut.bind(this)} >
                                    ออกจากระบบ
                                </NavLink>
                            </NavItem>

                        </Nav>
                    </Collapse>
                </Navbar>
                
            </div >
        )

        return (

            <div>
                {localStorage.user_token ? userLink : loginRegLink}
            </div >
        )
    }
}
export default withRouter(Nav_bar);