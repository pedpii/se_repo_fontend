import React, { Component } from 'react';
import { Button, Form, FormGroup, Label, Input, CardImg } from 'reactstrap';
import { post } from '../../service/service';
import { Redirect } from 'react-router-dom'
import {user_token} from '../../support/Constance'
class Add_product extends Component {

    constructor(props) {
        super(props);
        this.state = {
            pro_image : null,
            pro_name : null,
            pro_cost : null,
            pro_price : null,
            pro_amount : null,
            pro_details : null,
            pro_status : null,
            st : "gfbdfghdfghjdfhgjdfxj"

        }
    }

    uploadpicture = (e) => {

        let reader = new FileReader();
        let file = e.target.files[0];
        if (!file) {

        } else {
            reader.readAsDataURL(file)

            reader.onloadend = () => {
                console.log("img", reader.result)
                this.setState({
                    pro_image : reader.result
                });
            }
        }

    }

    add_product = async () => {
        let object = {
            pro_image: this.state.pro_image,
            pro_name: this.state.pro_name,
            pro_cost: this.state.pro_cost,
            pro_price: this.state.pro_price,
            pro_amount: this.state.pro_amount,
            pro_details: this.state.pro_details,
            pro_status: this.state.pro_status

        };

        try {
            await post(object, "product/add_product", null).then(res => {
                console.log("add_product" + res);

                if (res.success) {
                    alert("เพิ่มสินค้าเรียบร้อย");
                    window.location.href = "/product";
                } else {
                    alert(res.error_message);
                }
            });
        } catch (error) {
            alert(error);
        }
        console.log("product" + this.state);
    }

    render() {

        if(!user_token){
            return(<Redirect to="/"/>)
        }

        return (
            <div className="App">
                <h1> เพิ่มสินค้า </h1>

                <Form>
                    <FormGroup>
                        <Label>ชื่อสินค้า</Label>
                        <Input placeholder="กรุณากรอกชื่อสินค้า"
                            id="pro_name" name="pro_name"
                            type="text" 
                            onChange={(e) => this.setState({pro_name:e.target.value})} />
                    </FormGroup>
                    <FormGroup>
                        <Label>สถานะ</Label>
                        <Input placeholder="กรุณากรอกสถานะ เช่น มีสินค้า สินค้าพรีออเดอร์"
                            id="pro_status" name="pro_status"
                            type="text" 
                            onChange={(e) => this.setState({pro_status:e.target.value})} />
                    </FormGroup>
                    <FormGroup>
                        <Label>ราคาทุน</Label>
                        <Input type="number"
                            id="pro_cost" name="pro_cost" 
                            onChange={(e) => this.setState({pro_cost:e.target.value})} />
                    </FormGroup>
                    <FormGroup>
                        <Label>ราคาขาย</Label>
                        <Input type="number"
                            id="pro_price" name="pro_price" 
                            onChange={(e) => this.setState({pro_price:e.target.value})} />
                    </FormGroup>
                    <FormGroup>
                        <Label>จำนวนสินค้าในสต๊อก</Label>
                        <Input type="number"
                            id="pro_amount" name="pro_amount" 
                            onChange={(e) => this.setState({pro_amount:e.target.value})} />
                    </FormGroup>
                    <FormGroup>
                        <Label>รายละเอียด</Label>
                        <Input type="textarea"
                            id="pro_details" name="pro_details"
                            onChange={(e) => this.setState({pro_details:e.target.value})} />
                    </FormGroup>
                    <FormGroup>
                        <Label>รูปสินค้า</Label>
                        <Input type="file" onChange={(e) => this.uploadpicture(e)}
                        />
                    </FormGroup>
                    {this.state.pro_image ?
                        <CardImg top width="50%" src={this.state.pro_image} />
                        : null
                    }

                    <Button onClick={() => this.add_product(
                        this.state.pro_image,
                        this.state.pro_name,
                        this.state.pro_cost,
                        this.state.pro_price,
                        this.state.pro_amount,
                        this.state.pro_details,
                        this.state.pro_status,
                    
                    )} color="success">
                        บันทึก
                    </Button>
                <Button color="danger">ยกเลิก</Button>
                </Form>

            </div >
        )
    }
}
export default Add_product;