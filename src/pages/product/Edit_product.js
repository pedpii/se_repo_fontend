import React, { Component } from 'react';
import { post, ip} from '../../service/service';
import { user_token } from '../../support/Constance';
import { Button, Form, FormGroup, Label, Input ,CardImg } from 'reactstrap';


class Edit_product extends Component {
    constructor(props) {
        super(props);
        this.state = {
            product_data: [],
            pro_name: null,
            pro_amount: null,
            pro_cost: null,
            pro_details: null,
            pro_price: null,
            pro_status: null,
            pro_id: null,
            pro_image:null,
            default_image:"https://muaythaiauthority.com/wp-content/uploads/2014/10/default-img.gif"
        };
        this.handleInputChange = this.handleInputChange.bind(this);
    }

    componentWillMount() {
        this.get_product()
        console.log("product 1", this.props.location.params)
        this.setState(this.props.location.params)
    }

    get_product = async (pro_id) => {
        const object = {
            pro_id: pro_id
        }
        console.log(pro_id)
        if (pro_id) {
            try {
                await post(object, 'product/show_product_info', user_token).then((result) => {
                    if (result.success) {
                        this.setState({
                            product_data: result.result
                        })
                        setTimeout(() => {
                            console.log("get_product", result.result)
                        }, 500)
                    } else {
                        //window.location.href = "/";
                        alert(result.error_message)
                    }
                });
            } catch (error) {
                alert("get_product2" + error);
            }
        } else {
            console.log("1234")
        }
    }
    onSubmit(e) {
        const new_product = {
            pro_name: this.refs.pro_name.value,
            pro_amount: this.refs.state.pro_amount.value,
            pro_cost: this.refs.state.pro_cost.value,
            pro_details: this.refs.state.pro_details.value,
            pro_price: this.refs.state.pro_price.value,
            pro_status: this.refs.state.pro_status.value,
            pro_id: this.refs.state.pro_id.value,
            pro_image: this.refs.state.pro_image.value
        }
        this.edit_product(new_product);
        e.preventDefault();
    }

    updata(){
        this.setState ({
            pro_name: this.state.get_product.pro_name,
            pro_amount: this.state.get_product.pro_amount,
            pro_cost: this.state.get_product.pro_cost,
            pro_details: this.state.get_product.pro_details,
            pro_price: this.state.get_product.pro_price,
            pro_status: this.state.get_product.pro_status,
            pro_id: this.state.get_product.pro_id,
            pro_image: this.state.get_product.pro_image
        });
    }

    edit_product = async () => {

        let object = {
            pro_name: this.state.pro_name,
            pro_amount: this.state.pro_amount,
            pro_cost: this.state.pro_cost,
            pro_details: this.state.pro_details,
            pro_price: this.state.pro_price,
            pro_status: this.state.pro_status,
            pro_id: this.state.pro_id,
            pro_image: this.state.pro_image
        };
        console.log("image",this.state.pro_image)

        try {
            await post(object, "product/update_product_info", user_token)
                .then(res => {
                    console.log("edit1" + res);

                    if (res.success) {
                        alert("แก้ไขข้อมูลสินค้าเรียบร้อย");
                        window.location.href = "/product";
                    } else {
                        alert("edit_alert : " + res.error_message);
                    }
                });
        } catch (error) {
            alert(error);
        }
        console.log("edit2" + this.state);
    }

    handleInputChange(e) {
        const target = e.target;
        const value = target.value;
        const name = target.name;

        this.setState({
            [name]: value
        });
    }

    uploadpicture = (e) => {

        let reader = new FileReader();
        let file = e.target.files[0];
        if (!file) {

        } else {
            reader.readAsDataURL(file)

            reader.onloadend = () => {
                console.log("img", reader.result)
                this.setState({
                    pro_image : reader.result
                });
            }
        }

    }

    render() {
        return (
            <div className="App">
                <h1> แก้ไขข้อมูลสินค้า </h1>
                <Form onSubmit={this.onSubmit.bind(this)}>
                    <FormGroup>
                        <Label for="text" >ชื่อสินค้า</Label>
                        <Input type="text" name="pro_name" id="pro_name" value={this.state.pro_name}
                            onChange={this.handleInputChange} />
                    </FormGroup>
                    <FormGroup>
                        <Label for="text" >สถานะ</Label>
                        <Input type="text" name="pro_status" id="pro_status" value={this.state.pro_status}
                            onChange={this.handleInputChange} />
                    </FormGroup>
                    
                    <FormGroup>
                        <Label for="text">ราคาขาย</Label>
                        <Input type="text" name="pro_price" id="pro_price" value={this.state.pro_price}
                            onChange={this.handleInputChange} />
                    </FormGroup>
                    <FormGroup>
                        <Label for="number">จำนวนสินค้าในสต๊อก</Label>
                        <Input type="number" name="pro_amount" id="pro_amount" value={this.state.pro_amount}
                            onChange={this.handleInputChange} />
                    </FormGroup>
                    <FormGroup>
                        <Label for="text">รายละเอียด</Label>
                        <Input type="textarea" name="pro_details" id="pro_details" value={this.state.pro_details}
                            onChange={this.handleInputChange} />
                    </FormGroup>
                    <FormGroup>
                        <Label for="text">รูปสินค้า</Label>
                        <Input type="file" name="pro_image" id="pro_image" onChange={(e) => this.uploadpicture(e)}/>
                    </FormGroup>

                    {this.state.pro_image ?
                        <CardImg top width="50%" src={ip+this.state.pro_image} />
                        : null
                    }

                    <Button outline color="success"
                        onClick={() => this.edit_product(
                            this.state.pro_name,
                            this.state.pro_status,
                            this.state.pro_price,
                            this.state.pro_amount,
                            this.state.pro_details,
                            this.state.pro_id,
                            this.state.pro_cost,
                            this.state.pro_image
                        )}> บันทึก </Button>
                    <Button outline color="danger"> ยกเลิก </Button>
                </Form>

            </div>
        )
    }
}

export default Edit_product;
