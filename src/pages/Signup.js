import React, { Component } from 'react';
import '../App.css';
import { post } from '../service/service';
import { Button, Form, FormGroup, Label, Input, NavLink } from 'reactstrap';


class Signup extends Component {
    constructor(props) {
        super(props);
        this.state = {
            username: null,
            password: null,
            email: null,
            name: null,
            lastname: null,
            phone: null,
            address: null,
            user_type: 1
        };
    }

    handleChange = (e) => {
        this.setState({
            [e.target.id]: e.target.value
        })
    }

    show_password() {
        var show_password = document.getElementById("password");
        if (show_password.type === "password") {
            show_password.type = "text";
        } else {
            show_password.type = "password";
        }
    }

    add_user = async () => {
        let object = {
            username: this.state.username,
            password: this.state.password,
            email: this.state.email,
            name: this.state.name,
            lastname: this.state.lastname,
            phone: this.state.phone,
            address: this.state.address,
            user_type: this.state.user_type
        };

        try {
            await post(object, "user/user_register", null).then(res => {
                console.log("Signup" + res);

                if (res.success) {
                    alert("สร้างบัญชีผู้ใช้เรียบร้อย โปรดทำการเข้าสู่ระบบ");
                    window.location.href = "/signin";
                } else {
                    alert(res.error_message);
                }
            });
        } catch (error) {
            alert(error);
        }
        console.log("Signup" + this.state);
    }

    render() {
        return (
            <div className="App" >
                <h1> ลงทะเบียน </h1>
                <Form>
                    <FormGroup>
                        <Label for="name" > ชื่อ </Label>
                        <Input
                            type="text"
                            id="name"
                            className="FormField__Input"
                            placeholder="Enter your frist name"
                            name="name"
                            onChange={this.handleChange} />
                    </FormGroup>

                    <FormGroup>
                        <Label for="lastname" > นามสกุล </Label>
                        <Input
                            type="text"
                            id="lastname"
                            className="FormField__Input"
                            placeholder="Enter your last name"
                            name="lastname"
                            onChange={this.handleChange} />
                    </FormGroup>

                    <FormGroup>
                        <Label for="email" > E-mail </Label>
                        <Input
                            type="email"
                            id="email"
                            className="FormField__Input"
                            placeholder="Enter your E-mail"
                            name="email"
                            onChange={this.handleChange} />
                    </FormGroup>

                    <FormGroup>
                        <Label for="phone" > เบอร์โทรศัพท์ </Label>
                        <Input
                            type="tel"
                            id="phone"
                            className="FormField__Input"
                            placeholder="Enter your phone"
                            name="phone"
                            onChange={this.handleChange} />
                    </FormGroup>

                    <FormGroup>
                        <Label for="address" > ที่อยู่ </Label>
                        <Input
                            type="text"
                            id="address"
                            className="FormField__Input"
                            placeholder="Enter your address"
                            name="address"
                            onChange={this.handleChange} />
                    </FormGroup>

                    <FormGroup>
                        <Label for="username" > ชื่อผู้ใช้ </Label>
                        <Input
                            type="text"
                            id="username"
                            className="FormField__Input"
                            placeholder="Enter your username"
                            name="username"
                            onChange={this.handleChange} />
                    </FormGroup>

                    <FormGroup>
                        <Label for="password" > รหัสผ่าน </Label>
                        <Input
                            type="password"
                            id="password"
                            className="FormField__Input"
                            placeholder="Enter your password"
                            name="password"
                            onChange={this.handleChange} />

                    </FormGroup>
                    <FormGroup check>
                        <Label check>
                            <Input
                                type="checkbox"
                                id="show_password"
                                className="FormField__CheckboxLabel"
                                name="show_password"
                                onClick={this.show_password} />
                            แสดงรหัสผ่าน
                        </Label>
                    </FormGroup>
                    <FormGroup>
                        <Button
                            onClick={
                                () => this.add_user(
                                    this.state.username,
                                    this.state.password,
                                    this.state.email,
                                    this.state.name,
                                    this.state.last_name,
                                    this.state.phone,
                                    this.state.address,
                                    this.state.user_type
                                )
                            } color="primary">
                            ลงทะเบียน </Button>
                        <NavLink href="/signin" > มีบัญชีผู้ใช้เเล้ว </NavLink>
                    </FormGroup>
                </Form>
            </div>
        );
    }
}

export default Signup;
