import React, { Component } from 'react';
import {
  Button
} from 'reactstrap';
import '../../src/App.css';
class test extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isOpen: false
    };
    // this.toggle = this.toggle.bind(this);
  }

  openPage = (pageName, color) => {
    var i, tabcontent, tablinks;
    tabcontent = document.getElementsByClassName("tabcontent");
    for (i = 0; i < tabcontent.length; i++) {
      tabcontent[i].style.display = "none";
    }
    tablinks = document.getElementsByClassName("tablink");
    for (i = 0; i < tablinks.length; i++) {
      tablinks[i].style.backgroundColor = "";
    }
    document.getElementById(pageName).style.display = "block";
    // elmnt.style.backgroundColor = color;
  }

  render() {
    return (
      <div>
        {/* {() => this.delete(element.pro_id,index)} */}
        <Button onclick={() => this.openPage('Home', 'red')}>Home</Button>
        <Button onclick={() => this.openPage('News', 'green')}id="defaultOpen">News</Button>
        <Button onclick={() => this.openPage('Contact','blue')}>ontact</Button>
        <Button onclick={() => this.openPage('About', 'orange')}>About</Button>

        <div id="Home" className="tabcontent">
          <h3>Home</h3>
          <p>Home is where the heart is..</p>
        </div>

        <div id="News" className="tabcontent">
          <h3>News</h3>
          <p>Some news this fine day!</p>
        </div>

        <div id="Contact" className="tabcontent">
          <h3>Contact</h3>
          <p>Get in touch, or swing by for a cup of coffee.</p>
        </div>

        <div id="About" className="tabcontent">
          <h3>About</h3>
          <p>Who we are and what we do.</p>
        </div>
      </div>
    );
  }
}
export default test;