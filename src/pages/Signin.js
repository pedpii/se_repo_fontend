import React, { Component } from 'react';
import '../App.css';
import { get, post } from '../service/service';
import { Button, Form, FormGroup, Label, Input, Nav, NavLink } from 'reactstrap';

class Signin extends Component {
    constructor(props) {
        super(props);
        this.state = {
            username: null,
            password: null
        }
    }

    handleChange = (e) => {
        this.setState({
            [e.target.id]: e.target.value
        })
    }

    handleSubmit = (e) => {
        e.preventDefault();
        console.log("Signin" + this.state);
    }

    get_users = async () => {
        let object = {
            username: this.state.username,
            password: this.state.password
        };
        try {
            await post(object, "user/user_login", null).then(res => {
                if (res.success) {
                    // alert(res.token)
                    localStorage.setItem("user_token", res.token);
                    window.location.href = "/";
                    console.log("Signin" + res.token);
                } else {
                    alert(res.error_message);
                }
            });
        } catch (error) {
            alert(error);
        }
        console.log("Signin" + this.state);
    }

    facebook = async () => {
        console.log("facebook");
        try {
            await get("auth/facebook", null).then(res => {
                if (res.success) {
                    //localStorage.setItem("user_token",res.token);
                    //window.location.href = "/";
                    console.log("facebook : " + res.token);
                }
                else {
                    window.location.href = "/signup";
                }
            });
        } catch (error) {
            alert("alert_facebook" + error);
        }
    }


    render() {
        return (
            <div className="App" >
                <h1> เข้าสู่ระบบ </h1>
                <Form onSubmit={this.handleSubmit} >
                    <FormGroup>
                        <Label for="username" > ชื่อผู้ใช้งาน </Label>
                        <Input type="text" id="username" placeholder="Enter your username" name="username" onChange={this.handleChange} />
                    </FormGroup >

                    <FormGroup>
                        <Label for="password" > รหัสผ่าน </Label>
                        <Input type="password" id="password" placeholder="Enter your password" name="password" onChange={this.handleChange} />
                    </FormGroup>

                    <Button onClick={() => this.get_users(this.state.username, this.state.password)} color="primary">
                        เข้าสู่ระบบ
                    </Button>
                    <Nav>
                        <NavLink  href="/signup">สร้างบัญชีผู้ใช้</NavLink>
                    </Nav>
                    
                </Form>

            </div>
        );
    }
}

export default Signin;