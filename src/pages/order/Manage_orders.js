import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import {Button,Form,Table} from 'reactstrap';


class Manage_order extends Component {
   
    render() {
        return (
            <div className="App">
                <Form>
                    <h1>จัดการใบสั่งซื้อ</h1>
                    <Link to={{ pathname: "/create_order" }}><Button outline color="primary"  > สร้างใบสั่งซื้อ </Button></Link>
                    <br />
                    <Table>
                        <tbody>
                            <tr>
                                <th>ลำดับ</th>
                                <th>วันที่ใบสั่งซื้อ</th>
                                <th>เลขที่ใบสั่งซื้อ</th>
                                <th>รหัสผู้ขาย</th>
                                <th>ชื่อผู้ขาย</th>
                                <th>จำนวนเงิน</th>
                                <th>ผู้ชื่อรับ</th>
                                <th></th>
                            </tr>

                            <tr>
                                <td>1</td>
                                <td>21/6/2561</td>
                                <td>SE-21062019001</td>
                                <td>SE-1234</td>
                                <td>กอไก่ขัน ขอไข่กลม</td>
                                <td>20,000.00</td>
                                <td>คอควาย ไถ่นา</td>
                                <td>
                                    <Button outline color="primary"> แก้ไข </Button>
                                    <Button outline color="danger"> ลบ </Button>
                                </td>

                            </tr>

                            <tr>
                                <td>2</td>
                                <td>21/6/2561</td>
                                <td>SE-21062019002</td>
                                <td>SE-1234</td>
                                <td>กอไก่ขัน ขอไข่กลม</td>
                                <td>300,000.00</td>
                                <td>งองู ฉกปก</td>
                                <td>
                                    <Button outline color="primary"> แก้ไข </Button>
                                    <Button outline color="danger"> ลบ </Button>
                                </td>

                            </tr>

                        </tbody>
                    </Table>
                </Form>
            </div>
        )
    }
}

export default Manage_order;