import React, { Component } from 'react';
import {Col, Button, Form,Row,FormGroup, Input, Table} from 'reactstrap';
import {connect} from 'react-redux'

class ProductPayment extends Component {
    constructor(props){
        super(props);
        this.state={
            pro_data:[],
            pro_number:0,
            pro_id:null,
            pro_name:null,
            pro_unit:null,
            pro_amount:null,
            pro_price:null,
            pro_cost:null,
            pro_tax:null,       
            pro_total:null,
            pro_payment:null
        }
    }

    handleChange = (e) => {
        this.setState({
            [e.target.id]: e.target.value
        })
    }

    backTap = () => {
        this.props.nextTap('1',null)
    }

    nextTap = () => {
        this.props.PRODUCT_DATA(
            this.state.pro_id,
            this.state.pro_name,
            this.state.pro_unit,
            this.state.pro_amount,
            this.state.pro_price,
            this.state.pro_cost,
            this.state.pro_tax,
            this.state.pro_total,
            this.state.pro_payment)

            this.props.nextTap('3',null)
    }

    add_product = () => {
        this.props.ADD_DATA(
            this.state.pro_id,
            this.state.pro_name,
            this.state.pro_unit,
            this.state.pro_amount,
            this.state.pro_price,
            this.state.pro_cost=this.state.pro_amount*this.state.pro_price,
            this.state.pro_total=this.state.pro_total+this.state.pro_cost,
            this.state.pro_tax=this.state.pro_total*0.07,
            this.state.pro_payment=this.state.pro_total+this.state.pro_tax)

            let product_array = this.state.pro_data
            let object = {
                pro_id:this.state.pro_id,
                pro_name:this.state.pro_name,
                pro_unit:this.state.pro_unit,
                pro_amount:this.state.pro_amount,
                pro_price:this.state.pro_price,
                pro_cost:this.state.pro_cost,
                pro_tax:this.state.pro_tax,       
                pro_total:this.state.pro_total,
                pro_payment:this.state.pro_payment
            }

            product_array.push(object)
            this.setState({pro_data:product_array})


    }

    delete = (pro_cost,index) => {
        let pro_data_array = this.state.pro_data
        pro_data_array.splice(index,1)
        this.setState({pro_data:pro_data_array})

        this.props.DELETE_DATA(
            this.state.pro_id,
            this.state.pro_name,
            this.state.pro_unit,
            this.state.pro_amount,
            this.state.pro_price,
            this.state.pro_cost=this.state.pro_amount*this.state.pro_price,
            this.state.pro_total=this.state.pro_total-pro_cost,
            this.state.pro_tax=this.state.pro_total*0.07,
            this.state.pro_payment=this.state.pro_total+this.state.pro_tax)

    }

    render() {
        return (
            <div>
                <Form>
                    <Row form>
                        <Col sm="12" md={{ size: 6, offset: 2 }}>
                            <h4>รายการสินค้า</h4>
                        </Col>

                        <Col sm="12" md={{ size: 10, offset: 1 }}>
                            <FormGroup>

                                <Table>
                                    <tr>
                                        <th scope="row">ลำดับ</th>
                                        <th>รหัสสินค้า</th> {/*id*/}
                                        <th>ชื่อสินค้า</th> {/*name */}
                                        <th>จำนวน</th> {/*amount */}
                                        <th>หน่วยนับ</th> {/*unit */}
                                        <th>ราคา/หน่วย</th> {/*price*/}
                                        <th>รวม</th> {/*cost*/}
                                        <th></th>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td><Input type="text"
                                                id="pro_id" name="pro_id" 
                                                onChange = { this.handleChange }/></td>
                                        <td><Input type="text"
                                                id="pro_name" name="pro_name" 
                                                onChange = { this.handleChange }/></td>
                                        
                                        <td><Input type="number"
                                                id="pro_amount" name="pro_amount" 
                                                onChange = { this.handleChange }/></td>
                                        <td><Input type="text"
                                                id="pro_unit" name="pro_unit" 
                                                onChange = { this.handleChange }/></td>
                                        <td><Input type="number"
                                                id="pro_price" name="pro_price" 
                                                onChange = { this.handleChange }/></td>
                                        <td>{this.state.pro_cost=(this.state.pro_amount*this.state.pro_price).toFixed(2)}</td>
                                        <td><Button outline color="info" 
                                                onClick = {() => {this.add_product()}}>เพิ่มสินค้า</Button>
                                        </td>
                                    </tr>
                                    {
                                        this.state.pro_data.map((element, index) => {
                                            return (
                                                <tr>
                                                    <td>{index + 1}</td>
                                                    <td>{element.pro_id}</td>
                                                    <td>{element.pro_name}</td>
                                                    <td>{element.pro_amount}</td>
                                                    <td>{element.pro_unit}</td>
                                                    <td>{element.pro_price}</td>
                                                    <td>{element.pro_cost}</td>
                                                    <td><Button outline color="danger" onClick={() => this.delete(element.pro_cost,index)}> ลบ </Button></td>
                                                </tr>
                                            )
                                        })
                                    }
                                    <tr>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <th>รวม</th>
                                        <td>{(this.state.pro_total+0).toFixed(2)}</td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <th>ภาษีมูลค่าเพิ่ม 7%</th>
                                        <td>{(this.state.pro_total*0.07).toFixed(2)}</td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <th>รวมทั้งสิ้น</th>
                                        <td>{(this.state.pro_payment+0).toFixed(2)}</td>
                                        <td></td>
                                    </tr>        
                                        
                                    
                                </Table>

                                <hr className="my-2" />
                               
                            </FormGroup>
                        </Col>
                    </Row>
                    <Col sm="12" md={{ size: 6, offset: 3 }}>
                            <Button outline color="secondary" onClick = {() => {this.backTap()}}>ย้อนกลับ</Button>
                            <Button outline color="success" onClick = {() => {this.nextTap()}}>ถัดไป</Button>
                        </Col>
                </Form>
            </div>
        )
    }

}

const mapStateToProps = (state) => {
    return {
        OrderReducer : state.OrderReducer
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        PRODUCT_DATA: (pro_data,
            pro_id,
            pro_name,
            pro_unit,
            pro_amount,
            pro_price,
            pro_cost,
            pro_tax,
            pro_total,
            pro_payment) => {
                dispatch({
                    type:"PRODUCT_DATA",
                    payload:{
                        pro_data:[],
                        pro_id:pro_id,
                        pro_name:pro_name,
                        pro_unit:pro_unit,
                        pro_amount:pro_amount,
                        pro_price:pro_price,
                        pro_cost:pro_cost,
                        pro_tax:pro_tax,
                        pro_total:pro_total,
                        pro_payment:pro_payment
                    }
                })
            },
            ADD_DATA: (pro_data,
                pro_id,
                pro_name,
                pro_unit,
                pro_amount,
                pro_price,
                pro_cost,
                pro_tax,
                pro_total,
                pro_payment) => {
                    dispatch({
                        type:"ADD_DATA",
                        payload:{
                            pro_data:[],
                            pro_id:pro_id,
                            pro_name:pro_name,
                            pro_unit:pro_unit,
                            pro_amount:pro_amount,
                            pro_price:pro_price,
                            pro_cost:pro_cost,
                            pro_tax:pro_tax,
                            pro_total:pro_total,
                            pro_payment:pro_payment
                        }
                    })
                },
            DELETE_DATA: (pro_data,
                pro_id,
                pro_name,
                pro_unit,
                pro_amount,
                pro_price,
                pro_cost,
                pro_tax,
                pro_total,
                pro_payment) => {
                    dispatch({
                        type:"DELETE_DATA",
                        payload:{
                            pro_data:[],
                            pro_id:pro_id,
                            pro_name:pro_name,
                            pro_unit:pro_unit,
                            pro_amount:pro_amount,
                            pro_price:pro_price,
                            pro_cost:pro_cost,
                            pro_tax:pro_tax,
                            pro_total:pro_total,
                            pro_payment:pro_payment
                        }
                    })
                }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(ProductPayment);