import React, { Component } from 'react';
import {Col, Button, Form,Row,FormGroup, Input, Label} from 'reactstrap';
import {connect} from 'react-redux'

class AddressSend extends Component {
    constructor(props){
        super(props);
        this.state={
            send_name:null,
            send_address:null,
            send_postcode:null,
            send_phone:null,
            bearer_name:null,
            bearer_address:null,
            bearer_postcode:null,
            bearer_phone:null
            
        }
    }

    handleChange = (e) => {
        this.setState({
            [e.target.id]: e.target.value
        })
    }

    backTap = () => {
        this.props.nextTap('2',null)
    }

    nextTap = () => {
        this.props.DATA_ADDRESS(
            this.state.send_name,
            this.state.send_address,
            this.state.send_postcode,
            this.state.send_phone,
            this.state.bearer_name,
            this.state.bearer_address,
            this.state.bearer_postcode,
            this.state.bearer_phone
            )
        this.props.nextTap('4',null)
    }

    render() {
        return (
            <div>
                <Form>
                    <Row form>
                        <Col sm="12" md={{ size: 6, offset: 2 }}>
                            <h4>ที่อยู่ผู้ส่ง</h4>
                        </Col>
                        <Col sm="12" md={{ size: 6, offset: 3 }}>
                            <FormGroup>
                                <Label>ชื่อ</Label>
                                <Input type="text" placeholder="นางสาวดอเด็ก หัวใส" 
                                    name="send_name" id="send_name"
                                    onChange = { this.handleChange }/>
                                <Label>ที่อยู่ในเอกสาร</Label>
                                <Input type="text" placeholder="เลขที่ หมู ตำบล/แขวง อำเภอ/เขต จังหวัด"
                                    name="send_address" id="send_address"
                                    onChange = { this.handleChange }/>
                                <Label>รหัสไปรษณีย์</Label>
                                <Input type="number" 
                                    name="send_postcode" id="send_postcode"
                                    onChange = { this.handleChange }/>
                                <Label>เบอร์โทรศัพท์</Label>
                                <Input type="tel" placeholder="098-7654321" 
                                    name="send_phone" id="send_phone"
                                    onChange = { this.handleChange }/>
                            </FormGroup>
                        </Col>

                        <Col sm="12" md={{ size: 6, offset: 2 }}>
                            <h4>ที่อยู่ผู้รับ</h4>
                        </Col>
                        <Col sm="12" md={{ size: 6, offset: 3 }}>
                            <FormGroup>
                                <Label>ชื่อ</Label>
                                <Input type="text" placeholder="นางสาวดอเด็ก หัวใส" 
                                    name="bearer_name" id="bearer_name"
                                    onChange = { this.handleChange }/>
                                <Label>ที่อยู่ในเอกสาร</Label>
                                <Input type="text" placeholder="เลขที่ หมู ตำบล/แขวง อำเภอ/เขต จังหวัด" 
                                    name="bearer_address" id="bearer_address"
                                    onChange = { this.handleChange }/>
                                <Label>รหัสไปรษณีย์</Label>
                                <Input type="number"
                                    name="bearer_postcode" id="bearer_postcode"
                                    onChange = { this.handleChange }/>
                                <Label>เบอร์โทรศัพท์</Label>
                                <Input type="tel" placeholder="098-7654321"
                                    name="bearer_phone" id="bearer_phone"
                                    onChange = { this.handleChange }/>
                            </FormGroup>
                        </Col>
                    </Row>
                    <Col sm="12" md={{ size: 6, offset: 3 }}>
                            <Button outline color="secondary" onClick = {() => {this.backTap()}}>ย้อนกลับ</Button>
                            <Button outline color="success" onClick = {() => {this.nextTap()}}>ถัดไป</Button>
                        </Col>
                </Form>
            </div>
        )
    }

}

const mapStateToProps = (state) => {
    return {
        OrderReducer : state.OrderReducer
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        DATA_ADDRESS: (send_name,
            send_address,
            send_postcode,
            send_phone,
            bearer_name,
            bearer_address,
            bearer_postcode,
            bearer_phone) => {
                dispatch({
                    type:"DATA_ADDRESS",
                    payload:{
                        send_name:send_name,
                        send_address:send_address,
                        send_postcode:send_postcode,
                        send_phone:send_phone,
                        bearer_name:bearer_name,
                        bearer_address:bearer_address,
                        bearer_postcode:bearer_postcode,
                        bearer_phone:bearer_phone
                    }
                })
            }
        
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(AddressSend);