import React, { Component } from 'react';
import {Col, Button, Form,Row,FormGroup, Input, Label} from 'reactstrap';
import {connect} from 'react-redux'

class DataGeneral extends Component {
    constructor(props){
        super(props);
        this.state={
            order_number:null,
            order_date:null,
            order_saleman:null,
            payment_select:null,
            payment_money:null,
            payment_date:null,
            payment_time:null,
            payment_name:null,
            payment_agency:null,
        }
    }

    handleChange = (e) => {
        this.setState({
            [e.target.id]: e.target.value
        })
    }

    nextTap =()=> {
        this.props.DATA_GENERAL(
            this.state.order_number,
            this.state.order_date,
            this.state.order_saleman,
            this.state.payment_select,
            this.state.payment_money,
            this.state.payment_date,
            this.state.payment_time,
            this.state.payment_name,
            this.state.payment_agency)

            this.props.nextTap()
    }

    render() {
        return (
            <div>
                <Form>
                            <Row form>
                                <Col sm="12" md={{ size: 6, offset: 2 }}>
                                    <h4>ข้อมูลใบสั่งซื้อ</h4>
                                </Col>

                                <Col sm="12" md={{ size: 6, offset: 3 }}>
                                    <FormGroup>
                                        <Label>เลขที่ใบสั่งซื้อ</Label>
                                        <Input type="text" placeholder="SE-210620190001" 
                                            name="order_number" id="order_number"
                                            onChange = { this.handleChange }/>
                                    </FormGroup>
                                </Col>

                                <Col sm="12" md={{ size: 6, offset: 3 }}>
                                    <FormGroup>
                                        <Label>วันที่ใบสั่งซื้อ</Label>
                                        <Input type="date" placeholder="21/06/2019"
                                            name="order_date" id="order_date"
                                            onChange = { this.handleChange }/>
                                    </FormGroup>
                                </Col>

                                <Col sm="12" md={{ size: 6, offset: 3 }}>
                                    <FormGroup>
                                        <Label>ชื่อผู้ขาย</Label>
                                        <Input type="text" placeholder="นายกอไก่ขัน ขอไข่กลม" 
                                            name="order_saleman" id="order_saleman"
                                            onChange = { this.handleChange }/>
                                    </FormGroup>
                                </Col>
                            </Row>

                            <Row form>
                                <Col sm="12" md={{ size: 6, offset: 2 }}>
                                    <h4>ข้อมูลการจัดซื้อ</h4>
                                </Col>

                                <Col sm="12" md={{ size: 6, offset: 3 }}>
                                    <FormGroup>
                                        <Label>เงื่อนไขการชำระเงิน</Label>
                                        <Input type="text" placeholder="ชำระเงินสด โอนผ่านธนาคาร" 
                                            name="payment_select" id="payment_select"
                                            onChange = { this.handleChange }/>
                                    </FormGroup>
                                </Col>

                                <Col sm="12" md={{ size: 6, offset: 3 }}>
                                    <FormGroup>
                                        <Label>จำนวนเงิน</Label>
                                        <Input type="number" placeholder="20,000.00" 
                                            name="payment_money" id="payment_money"
                                            onChange = { this.handleChange }/>
                                        <Label>บาท</Label>
                                    </FormGroup>
                                </Col>

                                <Col sm="12" md={{ size: 6, offset: 3 }}>
                                    <FormGroup>
                                        <Label>วันที่ส่งของ</Label>
                                        <Input type="date" placeholder="25/06/2019" 
                                            name="payment_date" id="payment_date"
                                            onChange = { this.handleChange }/>
                                    </FormGroup>
                                </Col>

                                <Col sm="12" md={{ size: 6, offset: 3 }}>
                                    <FormGroup>
                                        <Label>เวลาที่ส่งของ</Label>
                                        <Input type="time" placeholder="10:00" 
                                            name="payment_time" id="payment_time"
                                            onChange = { this.handleChange }/>
                                    </FormGroup>
                                </Col>

                                <Col sm="12" md={{ size: 6, offset: 3 }}>
                                    <FormGroup>
                                        <Label>ชื่อผู้รับผิดชอบ</Label>
                                        <Input type="text" placeholder="นายงองู ฉกปก" 
                                            name="payment_name" id="payment_name"
                                            onChange = { this.handleChange }/>
                                    </FormGroup>
                                </Col>

                                <Col sm="12" md={{ size: 6, offset: 3 }}>
                                    <FormGroup>
                                        <Label>หน่วยงาน</Label>
                                        <Input type="text" placeholder="สาธารณะสุข" 
                                            name="payment_agency" id="payment_agency"
                                            onChange = { this.handleChange }/>
                                    </FormGroup>
                                </Col>
                            </Row>

                            <Col sm="12" md={{ size: 6, offset: 3 }}>
                            <Button outline color="success" onClick = {() => {this.nextTap()}}>ถัดไป</Button>
                        </Col>
                    </Form>
            </div>
        )
    }

}

const mapStateToProps = (state) => {
    return {
        OrderReducer : state.OrderReducer
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        DATA_GENERAL: (order_number,
                order_date,
                order_saleman,
                payment_select,
                payment_money,
                payment_date,
                payment_time,
                payment_name,
                payment_agency) => {
                dispatch({
                    type:"DATA_GENERAL",
                    payload:{
                        order_number:order_number,
                        order_date:order_date,
                        order_saleman:order_saleman,
                        payment_select:payment_select,
                        payment_money:payment_money,
                        payment_date:payment_date,
                        payment_time:payment_time,
                        payment_name:payment_name,
                        payment_agency:payment_agency
                    }
                })
            }
        
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(DataGeneral);