import React, { Component } from 'react';
import classnames from 'classnames';
import {Col, Button, Form,TabContent, TabPane, Nav, NavItem, NavLink,Row,FormGroup, Label, Table} from 'reactstrap';
import DataGeneral from './DataGeneral';
import ProductPayment from './ProductPayment';
import AddressSend from './AddressSend';
import { connect } from 'react-redux'


class Create_order extends Component {
    constructor(props) {
        super(props);
        this.toggle = this.toggle.bind(this);
        this.state = {
            pro_data:[],
            activeTab: '1',
            photo:"http://www.prosoftmyaccount.com/FileSystem/Image/phairintr/20101123/PO-%E0%B8%AA%E0%B8%B1%E0%B9%88%E0%B8%87%E0%B8%8B%E0%B8%B7%E0%B9%89%E0%B8%ADfrom.png"
        };
    }

    toggle(tab,back) {
        if(tab){
            if (this.state.activeTab !== tab) {
            this.setState({
                activeTab: tab
            });
        }

        }
        if(back)
        this.setState({
            activeTab: back
        });
        
    }

    render() {
        return (
            <div>
                <h1>สร้างใบสั่งซื้อ</h1>
                <Nav tabs>
                    <NavItem>
                        <NavLink className={classnames({ active: this.state.activeTab === '1' })} >
                            ข้อมูลทั่วไป
                        </NavLink>
                    </NavItem>

                    <NavItem>
                        <NavLink className={classnames({ active: this.state.activeTab === '2' })} >
                            สินค้า
                        </NavLink>
                    </NavItem>

                    <NavItem>
                        <NavLink className={classnames({ active: this.state.activeTab === '3' })} >
                            ที่อยู่เอกสาร
                        </NavLink>
                    </NavItem>

                    <NavItem>
                        <NavLink className={classnames({ active: this.state.activeTab === '4' })}>
                            ดูตัวอย่าง
                        </NavLink>
                    </NavItem>
                </Nav>

                {/* Form data */}
                <TabContent activeTab={this.state.activeTab}>
                    <TabPane tabId="1">
                        <DataGeneral nextTap = {() => {this.toggle('2')}}/>
                    </TabPane>

                    <TabPane tabId="2">
                        <ProductPayment nextTap = {(tab,back) => {this.toggle(tab,back)}}/>
                    </TabPane>

                    <TabPane tabId="3">
                        <AddressSend nextTap = {(tab,back) => {this.toggle(tab,back)}}/>
                    </TabPane>

                    <TabPane tabId="4">
                        <Form>
                            <Row form>
                                <Col sm="12" md={{ size: 6, offset: 2 }}>
                                    <h4>ตัวอย่าง</h4>
                                </Col>
                                <Col sm="12" md={{ size: 6, offset: 3 }}>
                                    <FormGroup>
                                        <Label>ตัวอย่างใบเสร็จแบบ PDF</Label>
                                    </FormGroup>

                                    <h4>ข้อมูลทั่วไป</h4>
                                    <Table>
                                        <tr>
                                            <th>เลขที่ใบสั่งซื้อ</th>
                                            <td>{this.props.OrderReducer.order_number}</td>
                                        </tr>
                                        <tr>
                                            <th>วันที่ใบสั่งซื้อ</th>
                                            <td>{this.props.OrderReducer.order_date}</td>
                                        </tr>
                                        <tr>
                                            <th>ชื่อผู้ขาย</th>
                                            <td>{this.props.OrderReducer.order_saleman}</td>
                                        </tr>
                                        <tr>
                                            <th>เงื่อนไขการชำระเงิน</th>
                                            <td>{this.props.OrderReducer.payment_select}</td>
                                        </tr>
                                        <tr>
                                            <th>จำนวนเงิน</th>
                                            <td>{this.props.OrderReducer.payment_money}</td>
                                        </tr>
                                        <tr>
                                            <th>วันที่ส่งของ</th>
                                            <td>{this.props.OrderReducer.payment_date}</td>
                                        </tr>
                                        <tr>
                                            <th>เวลาที่ส่งของ</th>
                                            <td>{this.props.OrderReducer.payment_time}</td>
                                        </tr>
                                        <tr>
                                            <th>ชื่อผู้รับผิดชอบ</th>
                                            <td>{this.props.OrderReducer.payment_name}</td>
                                        </tr>
                                        <tr>
                                            <th>หน่วยงาน</th>
                                            <td>{this.props.OrderReducer.payment_agency}</td>
                                        </tr>
                                    </Table>

                                    <h4>รายการสินค้า</h4>
                                    <Table>
                                        <tr>
                                            <th scope="row">ลำดับ</th>
                                            <th>รหัสสินค้า</th> {/*id*/}
                                            <th>ชื่อสินค้า</th> {/*name */}
                                            <th>จำนวน</th> {/*amount */}
                                            <th>หน่วยนับ</th> {/*unit */}
                                            <th>ราคา/หน่วย</th> {/*price*/}
                                            <th>รวม</th> {/*cost*/}
                                        </tr>
                                        {this.state.pro_data}
                                        <tr>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <th>รวม</th>
                                            <td>{this.props.OrderReducer.pro_total}</td>
                                        </tr>
                                        <tr>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <th>ภาษีมูลค่าเพิ่ม 7%</th>
                                            <td>{this.props.OrderReducer.pro_tax.toFixed(2)}</td>
                                        </tr>
                                        <tr>
                                        <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <th>รวมทั้งสิ้น</th>
                                            <td>{this.props.OrderReducer.pro_payment}</td>
                                        </tr>                                         
                                    </Table>



                                    <h4>ที่อยู่เอกสาร</h4>
                                    <Table>
                                        <tr>
                                            <th>ชื่อผู้ส่ง</th>
                                            <td>{this.props.OrderReducer.send_name}</td>
                                        </tr>
                                        <tr>
                                            <th>ที่อยู่</th>
                                            <td>{this.props.OrderReducer.send_address}</td>
                                        </tr>
                                        <tr>
                                            <th>รหัสไปรษณีย์</th>
                                            <td>{this.props.OrderReducer.send_postcode}</td>
                                        </tr>
                                        <tr>
                                            <th>เบอร์โทรศัพท์</th>
                                            <td>{this.props.OrderReducer.send_phone}</td>
                                        </tr>
                                        <tr>
                                            <th>ชื่อผู้รับ</th>
                                            <td>{this.props.OrderReducer.bearer_name}</td>
                                        </tr>
                                        <tr>
                                            <th>ที่อยู่</th>
                                            <td>{this.props.OrderReducer.bearer_address}</td>
                                        </tr>
                                        <tr>
                                            <th>รหัสไปรษณีย์</th>
                                            <td>{this.props.OrderReducer.bearer_postcode}</td>
                                        </tr>
                                        <tr>
                                            <th>เบอร์โทรศัพท์</th>
                                            <td>{this.props.OrderReducer.bearer_phone}</td>
                                        </tr>
                                    </Table>
                                   
                            
                                    <FormGroup>
                                    <Col sm="12" md={{ size: 6, offset: 3 }}>
                                        <Button outline color="secondary" onClick = {() => {{this.toggle('3')}}}>ย้อนกลับ</Button>
                                        <Button outline color="success">บันทึก</Button>
                                        <Button outline color="danger">ยกเลิก</Button>
                                    </Col>
                                    </FormGroup>
                                </Col>
                            </Row>
                        </Form>
                    </TabPane>
                </TabContent>
            </div>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        OrderReducer : state.OrderReducer
    }
}

export default connect(mapStateToProps, null)(Create_order);