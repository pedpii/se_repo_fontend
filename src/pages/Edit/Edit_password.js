import React, { Component } from 'react';
import { post } from '../../service/service';
import { user_token } from '../../support/Constance';
import { Button, Form, FormGroup, Label, Input } from 'reactstrap';



class Edit_password extends Component {
    constructor(props) {
        super(props);
        this.state = {
            password: null,
            newpassword: null,
            c_newpassword: null
        }
    }

    handleChange = (e) => {
        this.setState({
            [e.target.id]: e.target.value
        })
    }

    handleSubmit = () => {
        // perform all neccassary validations
        if (this.state.newpassword !== this.state.c_newpassword) {
            alert("รหัสผ่านไม่ตรงกัน");
        } else {
            this.edit();
            // this.setState ({ _password : this.state.newpassword })
            // this.edit();
            // console.log("edit_submit : " + this.state.password + "   edit : " + this.state.newpassword + "   edit : " + this.state._password);
        }

    }

    edit = async () => {
        let object = {
            password: this.state.password,
            newpassword: this.state.newpassword
        };
        console.log("obj_pass : " + object.password + "   obj_newpass : " + object.newpassword + "   obj__pass : " + object._password)
        try {
            await post(object, "user/user_update_password", user_token)
                .then(res => {


                    if (res.success) {
                        alert("เปลี่ยนรหัสผ่านเรียบร้อย");
                        window.location.href = "/user";
                    } else {
                        alert("error 2 : " + res.error_message);
                    }
                });
        } catch (error) {
            alert("error3" + error);
        }
    }


    render() {
        return (
            <div className="App__Aside">


                <h1> เปลี่ยนรหัสผ่าน </h1>
                <Form onSubmit={this.handleSubmit}>
                    <FormGroup>
                        <Label for="password" > รหัสผ่านเก่า </Label>
                        <Input
                            type="password"
                            id="password"

                            placeholder="Enter your password"
                            name="password"
                            onChange={this.handleChange}
                        />
                    </FormGroup>
                    <FormGroup>
                        <Label for="password" > รหัสผ่านใหม่ </Label>
                        <Input
                            type="password"
                            id="newpassword"

                            placeholder="Enter your new password"
                            name="newpassword"
                            onChange={this.handleChange}
                        />
                    </FormGroup>

                    <FormGroup>
                        <Label for="password" > ยืนยันรหัสผ่านใหม่ </Label>
                        <Input
                            type="password"
                            id="c_newpassword"

                            placeholder="Enter your new password agian"
                            name="c_newpassword"
                            onChange={this.handleChange}
                        />
                    </FormGroup>
                </Form>
                <Button outline color="success"
                    onClick={
                        () => this.handleSubmit(
                            this.state.password,
                            this.state.newpassword,
                            this.state.c_newpassword
                        )
                    }>
                    ยืนยัน
                    </Button>
                    <Button outline color="danger">ยกเลิก</Button>
            </div>
        );
    }
}

export default Edit_password;
