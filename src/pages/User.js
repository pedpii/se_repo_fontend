import React, { Component } from 'react';
import { get, ip } from '../service/service';
import { user_token } from '../support/Constance';
import { Table, CardImg } from 'reactstrap';

class User extends Component {
    constructor(props) {
        super(props);
        this.state = {
            username: '',
            name: '',
            lastname: '',
            email: '',
            phone: '',
            address: '',
            user_type: 1,
            get_user: null,
            isInEdit: false,
            user_image: null,
            default_user_image: "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQ2S47oPrtWI_lK68iwye6EW3Q9GMRPoCQPw4vlObBssUl355pLMg"
        };
    }


    get_user = async () => {
        try {
            await get('show/show_user', user_token).then((result) => {
                if (result.success) {
                    this.setState({
                        get_user: result.result
                    })
                    setTimeout(() => {
                        console.log("get_user", result.result)
                    }, 500)
                } else {
                    window.location.href = "/";
                    //alert("user1"+result.error_message);
                }
            });
        } catch (error) {
            alert("get_user2" + error);
        }
    }

    componentWillMount() {
        this.get_user()
    }

    render() {
        return (
            <div className="App">

                <h1> ข้อมูลบัญชีผู้ใช้ </h1>
                <Table>
                    <tbody >
                        <tr >
                            <th > ชื่อผู้ใช้ </th>
                            <td >
                                <div>
                                    {this.state.get_user ? this.state.get_user.username : null}
                                </div>
                            </td>
                        </tr >

                        <tr >
                            <th > ชื่อ </th>
                            <td > {this.state.get_user ? this.state.get_user.name : null} </td>
                        </tr >

                        <tr >
                            <th > นามสกุล </th>
                            <td > {this.state.get_user ? this.state.get_user.lastname : null} </td>
                        </tr >

                        <tr >
                            <th> E - mail </th>
                            <td> {this.state.get_user ? this.state.get_user.email : null} </td>
                        </tr >

                        <tr >
                            <th > เบอร์โทรศัพท์ </th>
                            <td > {this.state.get_user ? this.state.get_user.phone : null} </td>
                        </tr >

                        <tr >
                            <th > ที่อยู่ </th>
                            <td > {this.state.get_user ? this.state.get_user.address : null} </td>
                        </tr >
                        <tr >
                            <th> รูปผู้ใช้ </th>
                            <td>{this.state.get_user ? 
                                <CardImg src={ip+this.state.get_user.user_image}/>
                                : <CardImg src={this.state.default_image}/>}
                            </td>
                        </tr>
                    </tbody>
                </Table >
            </div>
        )
    }
}

export default User;